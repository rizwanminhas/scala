package scala

import scala.BigInt
import scala.annotation.tailrec
import scala.math.BigInt.int2bigInt

object PascalsTriangle {
  
    def main(args: Array[String]) {
      
      val col = 10
      val row = 43

      var now = System.nanoTime
      println(s"recursivelyFindValueAt($col,$row):" + recursivelyFindValueAt(col,row))
      
      var slow = (System.nanoTime - now) / 1000
      println("%d microseconds".format(slow))
      
      println("\n----------------------------\n")
      
      now = System.nanoTime      
      println(s"quicklyFindValueAt($col,$row):" + quicklyFindValueAt(col,row))
      
      var quick = (System.nanoTime - now) / 1000
      println("%d microseconds".format(quick))
      
      println("\nquicklyFindValueAt is approximately " + slow / quick + " times faster than recursivelyFindValueAt!!!")
    }
    
    /**
     * 
     */
	def recursivelyFindValueAt (column: Int, row: Int) : Int = {
		if (column == 0 || column == row) 1
		else recursivelyFindValueAt(column-1, row-1) + recursivelyFindValueAt(column, row-1)
	}
	
	/**
	 * 
	 */
	def quicklyFindValueAt (column: Int, row: Int) : BigInt = {
	  if (column == 0 || column == row) 1
	  else factorial(row) / ( factorial(column) * factorial(row - column) )
	  
	}
	
	/**
	 * 
	 */
	@tailrec
	def factorial (num: Int, value: BigInt = 1) : BigInt = {
	  if (num == 0 || num == 1 ) value
	  else factorial (num-1, value * num)
	}
}